# SETUP  
  
## JSON Server setup  
Raw data (setup from 0)  
```
Install JSON Server de manera global al nostre PC, npm install -g json-server
Installem el JSON server a un projecte local , i Per evitar que ens faci totes les preguntes fem directament:  npm install -y 
Instalem les dependecies al nostre fake server,  npm i json-server
Creem la BDs db.json
Arrancar el servidor JSON   json-server --watch ./db/db.json
```
  
Once set up  
```
$ npm i json-server
$ json-server --watch ./db/db.json
```
  
## Vue API init  
Raw data (setup from scratch)  
* pull from git repo  
  
Once set up  
```
# on root directory
$ npm install
$ npm run serve
```
