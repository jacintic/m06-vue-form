# Examen:  
1. Primero he añadido la opción de "offer" al CRUD de la vista Tutorial.vue.  
2. He colocado condicionales en las paginas chequeando que `offer == 1` que muestran una chapita de oferta.
3. He creado una vista Offers.vue. He hecho un getAll() que despues he filtrado por si `offer == 1` i he creado un nuevo array con esos objetos i los he mostrado en la lista `tutorials[]` del data de Offers.vue
4. He necesitado hacer un control de los tutoriales comprados o no por lo que he optado por hacer otra lista de ids nadamas de tutoriales comprados. En cada elemento del tutorial en la vista `Offers.vue` reviso si la `id` del tutorial esta en comprados i decido asi mostrar la opcion de compra o retirar compra.
  
  
# 'Remove all' buttons:  
Only one working is remove all from cart.
