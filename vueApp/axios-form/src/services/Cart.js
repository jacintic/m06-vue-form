import http from "../http-common";
class Cart {
  getAll() {
    return http.get("/cart");
  }

  get(id) {
    return http.get(`/cart/${id}`);
  }

  add(data) {
    return http.post("/cart", data);
  }

  update(id, data) {
    return http.put(`/cart/${id}`, data);
  }

  delete(id) {
    return http.delete(`/cart/${id}`);
  }

  deleteAll(idList) {
    idList.forEach(el => this.delete(el));
    return http.delete(`/cart/`);
  }

  findByTitle(id) {
    return http.get(`/cart?id=${id}`);
  }
}

export default new Cart();
